package oojava1.m05.ex05_person;

enum Gender {Male, Female}

public class Person {
	private Date birthday;
	private String name;
	private Gender gender;
	
	public Person() {
		
	}
	
	public Person(String name, Date birthday, Gender gender) {
		this.name = name;
		this.birthday = birthday;
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	
	

}
