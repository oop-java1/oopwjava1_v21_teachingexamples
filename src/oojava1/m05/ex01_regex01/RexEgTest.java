package oojava1.m05.ex01_regex01;

import java.util.Scanner;

public class RexEgTest {

	public static void main(String[] args) {
		String s;
		while (true) {
			System.out.println("Enter A-D");
			String txt = new Scanner(System.in).nextLine();
			System.out.println((txt.matches("[A-D]")) ? "YES": "NO");
		}

	}

}
