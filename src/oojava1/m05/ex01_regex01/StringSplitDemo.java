package oojava1.m05.ex01_regex01;

public class StringSplitDemo {

	public static void main(String[] args) {
		String str = "I am a string. I am to be split...";
		String[] words;
		System.out.println(str);
		// Splitting on " "
		words = str.split(" ");
		for (int i = 0; i<words.length; i++)
			System.out.print(words[i]+"/");
		System.out.println();
		// Splitting on " "
		words = str.split("(\\.\\s|\\s|\\.)");
		for (int i = 0; i<words.length; i++)
			System.out.print(words[i]+"/");
		System.out.println();
		
	}

}
