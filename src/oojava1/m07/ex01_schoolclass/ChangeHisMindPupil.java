package oojava1.m07.ex01_schoolclass;

public class ChangeHisMindPupil extends Pupil {
	
	public ChangeHisMindPupil(String name) {
		super(name);
	}
	
	@Override
	public void sayMyName() {
		super.sayMyName();
		System.out.println("No, sorry, my name is "+this.name+".");
	}

}
