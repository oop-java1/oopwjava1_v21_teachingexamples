package oojava1.m07.ex01_schoolclass;

public class SchoolClassTest {

	public static void main(String[] args) {
		SchoolClass schoolClass = new SchoolClass(1);
		Teacher oojava1Teacher = new Teacher();
		oojava1Teacher.setName("Jari");
		oojava1Teacher.setSubject("Java");
		oojava1Teacher.setSalary(100_000.02);
		schoolClass.setTeacher(oojava1Teacher);
		System.out.println("OOJava1 Teacher: "+oojava1Teacher);
		System.out.println("First class: "+schoolClass);
		
		oojava1Teacher.sayMyName();
		
		Pupil pupil1 = new Pupil("Jim");
		pupil1.sayMyName();
		
		ChangeHisMindPupil pupil2 = new ChangeHisMindPupil("John");
		pupil2.sayMyName();
		

	}

}
