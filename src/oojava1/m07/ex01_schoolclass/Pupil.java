package oojava1.m07.ex01_schoolclass;

import java.util.Date;

public class Pupil extends Person {
	private SchoolID id;
	
	public Pupil(String name) {
		super(name);
	}
	
	public SchoolID getId() {
		return id;
	}
	public void setId(SchoolID id) {
		this.id = id;
	}
	
	@Override
	public void sayMyName() {
		System.out.println("My name is "+this.name+"!!!!!!!!");
	}
	
	

}

	