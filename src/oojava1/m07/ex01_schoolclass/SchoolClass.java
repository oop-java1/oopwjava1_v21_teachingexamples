package oojava1.m07.ex01_schoolclass;

import java.util.ArrayList;

public class SchoolClass {
	private final int id;
	private ArrayList<Pupil> pupils;
	private Room room;
	private Teacher teacher;
	
	public SchoolClass(int id) {
		this.id = id;
		pupils = new ArrayList<Pupil>();
		
	}
	
	
	public ArrayList<Pupil> getPupils() {
		return pupils;  
	}
	public void setPupils(ArrayList<Pupil> pupils) {     
		this.pupils = pupils;
	}   
	public Room getRoom() {
		return room;
	}
	public void setRoom(Room room) {
		this.room = room;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public int getId() {
		return id;
	}
	
	public String toString() {
		return String.format("SchoolClass [%d] with %d pupils. Teacher is %s in %s. The salary is %f kr.",
				id,pupils.size(),teacher,teacher.getSubject(),teacher.getSalary());
	}
	
	

}
