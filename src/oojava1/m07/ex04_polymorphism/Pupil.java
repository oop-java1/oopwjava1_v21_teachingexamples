package oojava1.m07.ex04_polymorphism;

import java.util.Date;

public class Pupil extends SchoolMember {
	private SchoolID id;
	
	public Pupil(String name) {
		super(name);
	}
	
	public SchoolID getId() {
		return id;
	}
	public void setId(SchoolID id) {
		this.id = id;
	}
	
	@Override
	public void sayMyName() {
		System.out.println("My name is "+this.name+"!!!!!!!!");
	}
	
	

}

	