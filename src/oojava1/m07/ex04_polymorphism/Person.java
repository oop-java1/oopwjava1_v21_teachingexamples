package oojava1.m07.ex04_polymorphism;

import java.util.Date;

enum Gender {
	
	Male {
		public String toString() { return "male";}
	}, 
	Female {
		public String toString() { return "female";}
	}
}

public class Person {
	protected Date birthday;
	protected String name;
	protected Gender gender;
	
	public Person() {
		this("");
	}
	
	public Person(String name) {
		this.name = name;
	}
	
	public Person(String name, Date birthday, Gender gender) {
		this.name = name;
		this.birthday = birthday;
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return String.format("My name is %s. I am a %s. I was born on %s and I am %s.", 
				name,this.getClass().getSimpleName(),"xxx",gender.toString());
	}
	
	public void sayMyName() {
		System.out.println("My name is "+this.name+".");
	}

	
	

}
