package oojava1.m07.ex04_polymorphism;

import java.util.Date;

public class SchoolMember extends Person {
	
	public SchoolMember() {
		
	}
	
	public SchoolMember(String name) {
		super(name);
	}

	public SchoolMember(String name, Date date, Gender gender) {
		super(name,date,gender);
	}
	
	@Override
	public void sayMyName() {
		System.out.println("My name is "+this.name+". I am a SchoolMember.");
	}

}
