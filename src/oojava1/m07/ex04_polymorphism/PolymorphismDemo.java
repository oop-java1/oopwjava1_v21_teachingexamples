package oojava1.m07.ex04_polymorphism;

import java.util.Date;

public class PolymorphismDemo {
	
	public static void main(String[] args) {
		Group group = new Group();
		group.add(new Person("Santa Claus",new Date(1877,12,23),Gender.Male));
		group.add(new Person("Santa Wife",new Date(1777,12,23),Gender.Female));
		group.add(new SchoolMember("John",new Date(2010,03,15),Gender.Male));
		System.out.println(group.toString());
	}

}
