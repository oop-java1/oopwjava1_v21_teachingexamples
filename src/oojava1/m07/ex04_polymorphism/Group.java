package oojava1.m07.ex04_polymorphism;

import java.util.ArrayList;

public class Group {
	private ArrayList<Person> persons = new ArrayList<Person>();
	
	public void add(Person person) {
		persons.add(person);
	}
	
	public int count() {
		return persons.size();
	}
	
	public String toString() {
		String result = "";
		for (int i  = 0; i<persons.size();i++) {
			result+=persons.get(i).toString()+"\n";
		}
		return result.trim();
	}
	
}
