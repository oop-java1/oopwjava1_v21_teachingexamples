package oojava1.m07.ex04_polymorphism;

public class Teacher extends SchoolMember {
	private String subject;
	private double salary;
	
	public Teacher() {
		
	}
	
	public Teacher(String name, String subject, double salary) {
		this.name = name;
		this.subject = subject;
		this.salary = salary;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	

	
	@Override
	public void sayMyName() {
		System.out.println("Hello Class! My name is "+this.name+".");
	}
	
	

}
