package oojava1.m03.ex03;

public class OurMath {
	
	public static double add(double x, double y) {
		return x + y;
	}
	
	public static double sub(double x, double y) {
		return x - y;
	}
	
	public static int factorial(int n) {
		int res = 1;
		for (int i = 2; i<=n;i++) {
			res *= i;
		}
		return res;
	}
	
	public static double max(double d1, double d2) {
		return Math.max(d1, d2);
	}
	
	public static double max(double d1, double d2, double d3) {
		return max(max(d1,d2),d3);
	}

}
