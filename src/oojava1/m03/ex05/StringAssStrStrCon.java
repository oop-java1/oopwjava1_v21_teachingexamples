package oojava1.m03.ex05;

public class StringAssStrStrCon {

	public static void main(String[] args) {
		int i = 42;
		String s1 = "This is a number: "+Integer.toString(42);
		String s2 = "This is a number: "+42;
		String s3 = ""+42; // Why does this give an error?
		
		String t1 = "This "+"is a test";

	}

}
