package oojava1.m03.ex04;

public class StaticTest {
	public static final String FILENAME = "src/oojava1/m03/ex04/FileLoader.java";
	
	public static void main(String[] args) {
		for (int i = 0; i<2; i++) {
			System.out.printf("===\n%s\n===\n", FileLoader.loadFile(FILENAME));
			System.out.printf("Bytes loaded: %d bytes.\n", FileLoader.bytesLoaded);
		}
	}

}
