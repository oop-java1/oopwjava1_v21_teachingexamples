package oojava1.m09.ex02_xmlserialization_person;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.bind.JAXB;

import oojava1.m03.ex04.FileLoader;


public class XMLGroupDemo {


	
	
	public static void main(String[] args) {
		String fileName = "src/oojava1/m09/ex02_xmlserialization_person/group.xml";
		Group group = new Group();
		group.add(new Person("Nelson Mandela"));
		group.add(new Person("Lord Nelson"));
		group.add(new Person("George Washington"));
		System.out.println(group.toString());
		try {
			BufferedWriter output = Files.newBufferedWriter(Paths.get(fileName));
			JAXB.marshal(group, output);
			System.out.println("Marshalling Done");
			System.out.println("=======");
			System.out.println("File "+fileName + " content:");
			System.out.println(FileLoader.loadFile(fileName));
			System.out.println("=======");
		} catch (Exception e) {
			System.out.println("EXCEPTION:"+e);
		}
		
		try {
			BufferedReader input = Files.newBufferedReader(Paths.get(fileName));
			Group group2= JAXB.unmarshal(input, Group.class);
			System.out.printf("Group2::\n"+group2);
		} catch (Exception e ) {
			System.out.println("EXCEPTION:"+e);
		}
	}


}
