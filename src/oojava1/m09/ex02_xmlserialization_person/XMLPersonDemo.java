package oojava1.m09.ex02_xmlserialization_person;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.bind.JAXB;

import oojava1.m03.ex04.FileLoader;


public class XMLPersonDemo {

	public static void main(String[] args) {
		String fileName = "src/oojava1/m09/ex02_xmlserialization_person/person.xml";
		Person person = new Person("Nelson Mandela");
		System.out.println(person);
		try {
			BufferedWriter output = Files.newBufferedWriter(Paths.get(fileName));
			JAXB.marshal(person, output);
			System.out.println("Marshalling Done");
			System.out.println("=======");
			System.out.println("File "+fileName + " content:");
			System.out.println(FileLoader.loadFile(fileName));
			System.out.println("=======");
		} catch (Exception e) {
			System.out.println("EXCEPTION:"+e);
		}
		
		try {
			BufferedReader input = Files.newBufferedReader(Paths.get(fileName));
			Person person2 = JAXB.unmarshal(input, Person.class);
			System.out.printf("Person:: "+person2);
		} catch (Exception e ) {
			System.out.println("EXCEPTION:"+e);
		}
	}

}
