package oojava1.m09.ex02_xmlserialization_person;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;

public class Group {
	@XmlElement(name="person")
	private ArrayList<Person> persons = new ArrayList<Person>();
	
	public void add(Person person) {
		persons.add(person);
	}
	
	public int count() {
		return persons.size();
	}
	
	public String toString() {
		String result = "";
		for (int i  = 0; i<persons.size();i++) {
			result+=persons.get(i).toString()+"\n";
		}
		return result.trim();
	}
	
}
