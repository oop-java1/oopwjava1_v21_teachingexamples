package oojava1.m09.ex03_exceptions;

import java.io.IOException;

public class ExceptionDemo {
	
	public static int div(int a, int b) throws ArithmeticException {
		return a/b;
	}
	
	public static String readError() throws IOException {
		throw new IOException("This is a dummy IOException.");
	}
	
	public static void callReadError() {
		try {
			readError();
		} catch (IOException ioException) {
			//...
		}
	}
	
	public static void callReadError2() throws IOException {
			readError();
	}
	

	public static void main(String[] args) {
		try {
			System.out.println("Starting...");
			System.out.println("res: 11 / 2  = "+div(11,2));
//			System.out.println("res: 11 / 0  = "+div(11,0));
			System.out.println("read file: "+readError());
//		} catch (Exception e) { // DON'T DO THIS!!!!
//			System.err.println("EX:"+e);
//		}
		} catch (ArithmeticException arithmeticException) {
			System.err.println("EX:"+arithmeticException);
		} catch (IOException ioException) {
			System.err.println("EX:"+ioException);
		}
		finally {
			System.out.println("We're done.");
		}
		
	
			
		

	}

}
