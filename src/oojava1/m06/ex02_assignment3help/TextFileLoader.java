package oojava1.m06.ex02_assignment3help;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TextFileLoader {
	private String loadPath;
	private String filename;
	private int errorCode = 0;
	
	/**
	 * Constructor for object {@link TextFileLoader}
	 * @param filename The filename of the file to be loaded. Can contain both a simple filename or a path.
	 */
	public TextFileLoader(String filename) {
		this("",filename);
	}
	/**
	 * Constructor for object {@link TextFileLoader}
	 * @param loadPath The relative or absolute path of the file to be loaded. Can contain "";
	 * @param filename The filename of the file to be loaded. Can contain both a simple filename or a path.
	 */
	public TextFileLoader(String loadPath, String filename) {
		setLoadPath(loadPath);
		setFilename(filename);
		
	}
	
	public String getFileContent(String filename) {
		setFilename(filename);
		return getFileContent();
	}
	
	public String getFileContent() {
		String fileContent = "";
		String fullFilename = loadPath+filename;  
		try {
			File f = new File(fullFilename);
			if (!f.exists()){
				throw new IOException(String.format("File \"%s\" not found.",fullFilename));
			}
			Path pathFullFilename = Path.of(fullFilename);
			fileContent = Files.readString(pathFullFilename);
		}
		catch (IOException ioe) {
			setErrorCode(1);
			System.out.printf("Exception loading file content (EC:%d): %s",getErrorCode(),ioe.toString());
		}
		return fileContent;
		
	}

	public String getLoadPath() {
		return loadPath;
	}

	private void setLoadPath(String loadPath) {
		this.loadPath = loadPath;
	}

	public int getErrorCode() {
		return errorCode;
	}

	private void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getFilename() {
		return filename;
	}

	private void setFilename(String filename) {
		this.filename = filename;
	}
	

}
