package oojava1.m06.ex02_assignment3help;

public class Simpletron {
	public static final int HALT = 43;
	
	private static final int MEMORY_SIZE = 100;
	private int[] memory = new int[MEMORY_SIZE];
	private int instructionCounter;
	private int instructionRegister;
	private int operationCode;
	private int operand;
	private int accumulator;
	
	/**
	 * Allows the user to enter a programme. When done, the method executes the programme.
	 */
	public void run() {
		
	}
	
	/**
	 * Loads and runs a sml program
	 * @param smlProgramText
	 */
	public void loadAndRunProgram(String smlProgramText) {
		
	}
	
	private void runProgramme() {
		do {
			fetch();
			runInstruction();
			
		} while (operationCode!=HALT);
	}
	
	private void runInstruction() {
		// TODO Auto-generated method stub
		
	}

	private void fetch() {
		instructionRegister = memory[instructionCounter];
		operationCode = instructionRegister / 100;
		operand = instructionRegister % 100;
	}
	
	private void dump() {
		
	}
	

	
	

}
