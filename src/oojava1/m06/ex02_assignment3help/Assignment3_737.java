package oojava1.m06.ex02_assignment3help;
import java.util.Scanner;

public class Assignment3_737 {
	private static String menu = ""
			+ "=== Simpletron menu ==="
			+ "Choices : \n"
			+ " 1 : Enter program.\n"
			+ " 2 : Load and run 734.sml\n"
			+ " 3 : Load and run 735.sml\n"
			+ " 4 : Load and run a.sml\n"
			+ " 5 : Load and run b.sml\n"
			+ " 6 : Load and run c.sml\n"
			+ "\n"
			+ "Enter choice:";
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int choice;
		String filename="";
		do {
			System.out.printf(menu);
			choice = input.nextInt();
			switch (choice) {
				case 2 : 
					filename = "734.sml";
					break;
				case 3 : 
					filename = "735.sml";
					break;
				case 4 : 
					filename = "a.sml";
					break;
				case 5 : 
					filename = "b.sml";
					break;
				case 6 : 
					filename = "c.sml";
					break;
			}
			if (choice==1) {
				Simpletron simpletron = new Simpletron();
				simpletron.run();
			}
			if (choice>1 && choice<7) {
				TextFileLoader textFileLoader = new TextFileLoader("Assignment3/",filename);
				String smlProgram = textFileLoader.getFileContent();
				Simpletron simpletron = new Simpletron();
				simpletron.loadAndRunProgram(smlProgram);
			}

				
		} while (choice!=0);
		
		
	}

}
