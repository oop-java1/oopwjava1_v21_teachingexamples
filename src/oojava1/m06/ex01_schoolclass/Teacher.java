package oojava1.m06.ex01_schoolclass;

public class Teacher {
	private String name;
	private String subject;
	private double salary;
	
	public Teacher() {
		
	}
	
	public Teacher(String name, String subject, double salary) {
		this.name = name;
		this.subject = subject;
		this.salary = salary;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public String toString() {
		return name;
	}
	
	

}
