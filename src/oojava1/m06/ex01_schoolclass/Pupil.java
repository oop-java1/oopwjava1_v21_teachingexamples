package oojava1.m06.ex01_schoolclass;

import java.util.Date;

enum Gender {Male, Female};

public class Pupil {
	private SchoolID id;
	private String name;
	private Gender gender;
	private Date birthdate;
	
	
	public SchoolID getId() {
		return id;
	}
	public void setId(SchoolID id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	

}
