package oojava1.m06.ex01_schoolclass;

public class SchoolClassTest {

	public static void main(String[] args) {
		SchoolClass schoolClass = new SchoolClass(1);
		Teacher oojava1Teacher = new Teacher();
		oojava1Teacher.setName("Jari");
		oojava1Teacher.setSubject("Java");
		oojava1Teacher.setSalary(100_000.02);
		schoolClass.setTeacher(oojava1Teacher);
		System.out.println("OOJava1 Teacher: "+oojava1Teacher);
		System.out.println("First class: "+schoolClass);
		
		schoolClass.getTeacher().setName("Hilmar");
		//Teacher oojava2Teacher
		oojava1Teacher = new Teacher();
		oojava1Teacher.setName("Hilmar");
		oojava1Teacher.setSubject("Java");
		schoolClass.setTeacher(new Teacher("Hans","SQL", 999999999));
		System.out.println("OOJava1 Teacher: "+oojava1Teacher);
		System.out.println("First class: "+schoolClass);
		

	}

}
