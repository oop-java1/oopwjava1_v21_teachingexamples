package oojava1.m14.ex03_search;

import java.util.Arrays;
import java.util.Random;

public class BigOLinearSearchDemo {
	private static long nEvals = 0;
	
	private static int randInt(int max) {
		return Math.abs(new Random().nextInt() % max);
	}
	
	private static int[] randomArray(int n, int max) {
		int[] intarray = new int[n];
		Random random = new Random(42);
		for (int i=0; i<n;i++) {
			intarray[i] = Math.abs(random.nextInt() % max);
		}
		return intarray;
	}

	public static void main(String[] args) {
		int N = 10;
		int[] a = randomArray(N,N);
		//Arrays.sort(a);
		int M = 1000;
		int[] b = randomArray(M,M);
		//Arrays.sort(b);
		int L = 100000;
		int[] c = randomArray(L,L);
		//Arrays.sort(c);
		
		int p ;
		System.out.println("Unsorted data set:");
		System.out.println(Arrays.toString(a));
		p = linearFind(randInt(a.length),a);
		System.out.printf("LinearFind:  pos = %5d. Number of evals (N = %7d): %d%n",p,N,nEvals);
		
		p = linearFind(randInt(b.length),b);
		System.out.printf("LinearFind:  pos = %5d. Number of evals (N = %7d): %d%n",p, M,nEvals);

		p = linearFind(randInt(c.length),c);
		System.out.printf("LinearFind:  pos = %5d. Number of evals (N = %7d): %d%n",p,L,nEvals);

	}


	private static int linearFind(int value, int[] searchArray) {
		nEvals = 0;
		int result = -1;
		for (int i=1; i<searchArray.length;i++) {
			nEvals++;
			if (searchArray[i]==value) {
				result = i;
				break;
			}
		}
		return result;
	}
	

}
