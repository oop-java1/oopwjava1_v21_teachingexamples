package oojava1.m14.ex03_search;

import java.util.Arrays;
import java.util.Random;

public class BigOTreeSearch {
	private static long nEvals = 0;
	
	private static int[] randomArray(int n, int max) {
		int[] intarray = new int[n];
		Random random = new Random(4);
		for (int i=0; i<n;i++) {
			intarray[i] = Math.abs(random.nextInt() % max);
		}
		return intarray;
	}

	public static void main(String[] args) {
		int N = 10;
		int[] a = randomArray(N,N);
		Arrays.sort(a);
		int M = 1000;
		int[] b = randomArray(M,M);
		Arrays.sort(b);
		int L = 100000;
		int[] c = randomArray(L,L);
		Arrays.sort(c);
		
		int p ;
		System.out.println(Arrays.toString(a));
		p = binaryFind(a.length/3,a);
		System.out.printf("BinaryFind:  pos = %5d. Number of evals (N = %7d): %d%n",p,N,nEvals);
		
		p = binaryFind(b.length/3,b);
		System.out.printf("BinaryFind:  pos = %5d. Number of evals (N = %7d): %d%n",p, M,nEvals);

		p = binaryFind(c.length/3,c);
		System.out.printf("BinaryFind:  pos = %5d. Number of evals (N = %7d): %d%n",p,L,nEvals);
		


	}


	private static int binaryFind(int value, int[] searchArray) {
		System.out.println("Value: "+value);
		nEvals = 0;
		int result = -1;
		int position = 0;
		int delta = (searchArray.length+1)/2;
		position = position + delta -1;
		do {
//			System.out.println("position "+position);
			nEvals++;
			if (value==searchArray[position]) {
				result = position;
			} else {
				delta = (delta +1) / 2;
//				System.out.println("delta "+delta);
				if (value < searchArray[position]) {
					position = Math.max(0,position - delta);
				} else {
					position = Math.min(searchArray.length-1, position + delta);
				}
			}
		} while (delta>1 && (result==-1));
		return result;
	}
	
	
	
	




}
