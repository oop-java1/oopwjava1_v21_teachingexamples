package oojava1.m14.ex04_bigosortingalgorithms;

import java.util.Arrays;
import java.util.Random;

public class BubbleSortDemo {
	private static long nEvalsSwaps = 0;
	
	private static int[] randomArray(int n, int max) {
		int[] intarray = new int[n];
		Random random = new Random(42);
		for (int i=0; i<n;i++) {
			intarray[i] = Math.abs(random.nextInt() % max);
		}
		return intarray;
	}
	
	public static void bubbleSort(int[] a) {
		nEvalsSwaps = 0;
		int tmp;
		for (int i = 0; i<a.length; i++) {
			for (int j = 0; j<a.length-i-1; j++) {
				nEvalsSwaps++;
				if (a[j]>a[j+1]) { // the swap
					nEvalsSwaps+=3;
					tmp=a[j];
					a[j]=a[j+1];
					a[j+1]=tmp;
				}
			}
		}
	}

	public static void main(String[] args) {
		int N = 10;
		int[] a = randomArray(N,N);
		//Arrays.sort(a);
		int M = 1000;
		int[] b = randomArray(M,M);
		//Arrays.sort(b);
		int L = 100000;
		int[] c = randomArray(L,L);
		
		int p ;
		System.out.println("Unsorted data set:");
		System.out.println(Arrays.toString(a));
		bubbleSort(a);
		System.out.println(Arrays.toString(a));
		System.out.printf("BubbleSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",N,nEvalsSwaps,nEvalsSwaps*1.0);
		bubbleSort(b);
		System.out.printf("BubbleSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",M,nEvalsSwaps,nEvalsSwaps*1.0);
		bubbleSort(c);
		System.out.printf("BubbleSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",L,nEvalsSwaps,nEvalsSwaps*1.0);
		
	}

}
