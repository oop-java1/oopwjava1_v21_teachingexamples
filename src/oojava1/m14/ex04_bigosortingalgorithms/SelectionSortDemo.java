package oojava1.m14.ex04_bigosortingalgorithms;

import java.util.Arrays;
import java.util.Random;

public class SelectionSortDemo {
	private static long nEvalsSwaps = 0;
	
	private static int[] randomArray(int n, int max) {
		int[] intarray = new int[n];
		Random random = new Random(42);
		for (int i=0; i<n;i++) {
			intarray[i] = Math.abs(random.nextInt() % max);
		}
		return intarray;
	}
	
	public static void selectionSort(int[] a) {
		nEvalsSwaps = 0;
		int tmp, iMin;
		for (int i = 0; i<a.length-1; i++) {
			iMin=i;
			for (int j = i+1; j<a.length-1; j++) { // inner for loop (j)
				nEvalsSwaps++;
				if (a[iMin]>a[j]) {
					iMin=j;
				} // if
			} // inner for loop (j)
			if (iMin!=i) {
				nEvalsSwaps+=3;
				tmp=a[i];
				a[i]=a[iMin];
				a[iMin]=tmp;
			}
		}
	}

	public static void main(String[] args) {
		int N = 10;
		int[] a = randomArray(N,N);
		//Arrays.sort(a);
		int M = 1000;
		int[] b = randomArray(M,M);
		//Arrays.sort(b);
		int L = 100000;
		int[] c = randomArray(L,L);
		
		int p ;
		System.out.println("Unsorted data set:");
		System.out.println(Arrays.toString(a));
		selectionSort(a);
		System.out.println(Arrays.toString(a));
		System.out.printf("selectionSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",N,nEvalsSwaps,nEvalsSwaps*1.0);
		selectionSort(b);
		System.out.printf("selectionSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",M,nEvalsSwaps,nEvalsSwaps*1.0);
		selectionSort(c);
		System.out.printf("selectionSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",L,nEvalsSwaps,nEvalsSwaps*1.0);
//		
	}

}
