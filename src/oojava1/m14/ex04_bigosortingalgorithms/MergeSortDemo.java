package oojava1.m14.ex04_bigosortingalgorithms;

// Fig. 19.6: MergeSortTest.java
// Sorting an array with merge sort.
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

public class MergeSortDemo {
	private static long nEvalsSwaps;
	
	private static int[] randomArray(int n, int max) {
		int[] intarray = new int[n];
		Random random = new Random(42);
		for (int i=0; i<n;i++) {
			intarray[i] = Math.abs(random.nextInt() % max);
		}
		return intarray;
	}
	
	
   // calls recursive sortArray method to begin merge sorting
   public static void mergeSort(int[] data) {
      sortArray(data, 0, data.length - 1); // sort entire array
   }                                  

   // splits array, sorts subarrays and merges subarrays into sorted array
   private static void sortArray(int[] data, int low, int high) {
      // test base case; size of array equals 1    
	  nEvalsSwaps++;
      if ((high - low) >= 1) { // if not base case
    	  nEvalsSwaps+=2;
         int middle1 = (low + high) / 2; // calculate middle of array
         int middle2 = middle1 + 1; // calculate next element over     

         // split array in half; sort each half (recursive calls)
         sortArray(data, low, middle1); // first half of array       
         sortArray(data, middle2, high); // second half of array     

         // merge two sorted arrays after split calls return
         merge (data, low, middle1, middle2, high);             
      }                                            
   }                               
   
   // merge two sorted subarrays into one sorted subarray             
   private static void merge(int[] data, int left, int middle1, 
      int middle2, int right) {

      int leftIndex = left; // index into left subarray              
      int rightIndex = middle2; // index into right subarray         
      int combinedIndex = left; // index into temporary working array
      int[] combined = new int[data.length]; // working array        
      
      // merge arrays until reaching end of either         
      while (leftIndex <= middle1 && rightIndex <= right) {
         // place smaller of two current elements into result  
         // and move to next space in arrays
    	  nEvalsSwaps++;
         if (data[leftIndex] <= data[rightIndex]) {
        	 nEvalsSwaps++;
            combined[combinedIndex++] = data[leftIndex++]; 
         } 
         else {            
        	 nEvalsSwaps++;
            combined[combinedIndex++] = data[rightIndex++];
         } 
      } 
   
      // if left array is empty          
      nEvalsSwaps++;
      if (leftIndex == middle2) {                             
         // copy in rest of right array                        
         while (rightIndex <= right) {
        	 nEvalsSwaps++;
            combined[combinedIndex++] = data[rightIndex++];
         } 
      } 
      else { // right array is empty                             
         // copy in rest of left array                         
         while (leftIndex <= middle1) {
        	 nEvalsSwaps++;
            combined[combinedIndex++] = data[leftIndex++]; 
         } 
      } 

      // copy values back into original array
      for (int i = left; i <= right; i++) { 
    	  nEvalsSwaps++;
         data[i] = combined[i];          
      } 

   } 

   // method to output certain values in array
   private static String subarrayString(int[] data, int low, int high) {
      StringBuilder temporary = new StringBuilder();

      // output spaces for alignment
      for (int i = 0; i < low; i++) {
         temporary.append("   ");
      } 

      // output elements left in array
      for (int i = low; i <= high; i++) {
         temporary.append(" " + data[i]);
      } 

      return temporary.toString();
   }

   public static void main(String[] args) {
		int N = 10;
		int[] a = randomArray(N,N);
		//Arrays.sort(a);
		int M = 1000;
		int[] b = randomArray(M,M);
		//Arrays.sort(b);
		int L = 100000;
		int[] c = randomArray(L,L);
		
		int p ;
		System.out.println("Unsorted data set:");
		System.out.println(Arrays.toString(a));
		mergeSort(a);
		System.out.println(Arrays.toString(a));
		System.out.printf("mergeSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",N,nEvalsSwaps,nEvalsSwaps*1.0);
		mergeSort(b);
		System.out.printf("mergeSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",M,nEvalsSwaps,nEvalsSwaps*1.0);
		mergeSort(c);
		System.out.printf("mergeSort: Number of evals+assigns (N = %7d): %12d = %3.1e%n",L,nEvalsSwaps,nEvalsSwaps*1.0);
   	} 
} 


/**************************************************************************
 * (C) Copyright 1992-2018 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/