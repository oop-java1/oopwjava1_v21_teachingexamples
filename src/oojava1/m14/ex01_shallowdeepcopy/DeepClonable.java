package oojava1.m14.ex01_shallowdeepcopy;

public interface DeepClonable {
	/**
	 * Should be implemented to return a entirely deep copy clone of the object
	 * @return
	 */
	public Object deepClone();

}
