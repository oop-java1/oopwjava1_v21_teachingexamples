package oojava1.m14.ex01_shallowdeepcopy;

import java.util.Date;

enum Gender {
	
	Male {
		public String toString() { return "male";}
	}, 
	Female {
		public String toString() { return "female";}
	}
}

public class Person  implements DeepClonable{
	protected Date birthday;
	protected String name;
	protected Gender gender;
	
	
	public Person() {
		this("");
	}
	
	public Person(String name) {
		this(name,new Date(1918, 11, 11),Gender.Male);
	}
	
	public Person(String name, Date birthday, Gender gender) {
		this.name = name;
		this.birthday = birthday;
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return String.format("My name is %s.",name);
	}
	
	public void sayMyName() {
		System.out.println("My name is "+this.name+".");
	}

	/**
	 * Return a entirely deep copy clone of the object
	 * @return a deep clone of the object
	 */
	@Override
	public Object deepClone() {
		Person person = new Person();
		person.setName(this.getName());
		person.setBirthday(new Date(this.birthday.getYear(), 
				this.birthday.getMonth(),this.birthday.getDate()));
		person.setGender(this.getGender());
		return person;
	}

	
	

}
