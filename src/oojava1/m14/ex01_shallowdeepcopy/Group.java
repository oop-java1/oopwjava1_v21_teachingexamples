package oojava1.m14.ex01_shallowdeepcopy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Group implements Cloneable, DeepClonable {
	private String name;
	private Person president;
	private ArrayList<Person> persons = new ArrayList<Person>();
	
	public Group() {
		this("");
	}
	
	public Group(String name) {
		this.name = name;
	}
	/**
	 * Clone performs a shallow copy of the Object which is cloned
	 */
	@Override
	public Object clone() {
		Group grp = new Group();
		grp.setName(this.getName());
		grp.setPresident(this.getPresident());
		grp.setPersons(this.getPersons());
		return grp;
	}
	
	public void add(Person person) {
		persons.add(person);
	}
	
	public int count() {
		return persons.size();
	}
	
	public String toString() {
		String result = "";
		result += String.format("Group name: %s%n",getName());
		if (getPresident()!=null) {
			result += String.format("President : %s%n",getPresident().getName());
		}
		for (int i  = 0; i<persons.size();i++) {
			result+=persons.get(i).toString()+"\n";
		}
		return result.trim();
	}
	
	public void sort() {
		
		Comparator personComparator = new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};
	
		Collections.sort(persons,personComparator);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Person> getPersons() {
		return persons;
	}

	public void setPersons(ArrayList<Person> persons) {
		this.persons = persons;
	}
	
	public Person getPresident() {
		return president;
	}

	public void setPresident(Person president) {
		this.president = president;
	}

	/**
	 * Return a entirely deep copy clone of the object
	 * @return a deep clone of the object
	 */
	@Override
	public Object deepClone() {
		Group grp = new Group();
		grp.setName(this.getName());
		if (this.getPresident()!=null) {
			grp.setPresident((Person) this.getPresident().deepClone());
		}
		for (Person person : persons) {
			grp.add((Person) person.deepClone());
		}
		return grp;
	}
	
	
	
}
