package oojava1.m14.ex01_shallowdeepcopy;

import java.util.Date;

public class DeepCopyDemo {
	
	public static void main(String[] args) {
		Group group1 = new Group("Group 1");
		group1.setPresident(new Person("Joakim And",new Date(1903,03,04),Gender.Male));
		group1.add(new Person("Anders And",new Date(1911,11,11),Gender.Male));
		group1.add(new Person("Mickey Mouse",new Date(1913,12,12),Gender.Male));
		
		Group group2 = (Group) group1.deepClone();
		group2.setName("Group 2");
		System.out.println("\nAfter Clone::");
		System.out.println(group1.toString());
		System.out.println(group2.toString());
		
		group2.getPresident().setName("Georg Gearløs");
		group2.getPersons().get(0).setName("Fedtmule");
		System.out.println("\nAfter change to Group 2 objects::");
		System.out.println(group1.toString());
		System.out.println(group2.toString());
		
	}

}
