package oojava1.m14.ex02_bigointro;

import java.util.Arrays;
import java.util.Random;

public class BigODemo02Sorted {
	private static long nEvals = 0;
	
	private static int[] randomArray(int n, int max) {
		int[] intarray = new int[n];
		Random random = new Random(42);
		for (int i=0; i<n;i++) {
			intarray[i] = Math.abs(random.nextInt() % max);
		}
		return intarray;
	}

	public static void main(String[] args) {
		int N = 10;
		int[] a = randomArray(N,N);
		Arrays.sort(a);
		int M = 1000;
		int[] b = randomArray(M,M);
		Arrays.sort(b);
		int L = 100000;
		int[] c = randomArray(L,L);
		Arrays.sort(c);
		
		System.out.println("Sorted data sets");
		System.out.println(Arrays.toString(a));
		compareFirst(a);
		System.out.printf("FirstCompare:  Number of evaluations (N = %7d): %d%n",N,nEvals);
		
		compareFirst(b);
		System.out.printf("FirstCompare:  Number of evaluations (N = %7d): %d%n",M,nEvals);
		
		findFirstCopy(a);
		System.out.printf("FindFirstCopy: Number of evaluations (N = %7d): %d%n",N,nEvals);

		findFirstCopy(b);
		System.out.printf("FindFirstCopy: Number of evaluations (N = %7d): %d%n",M,nEvals);

		findFirstCopy(c);
		System.out.printf("FindFirstCopy: Number of evaluations (N = %7d): %d%n",L,nEvals);
		
		findAnyCopy(a);
		System.out.printf("FindAnyCopy:   Number of evaluations (N = %7d): %d%n",N,nEvals);

		findAnyCopy(b);
		System.out.printf("FindAnyCopy:   Number of evaluations (N = %7d): %d%n",M,nEvals);

		findAnyCopy(c);
		System.out.printf("FindAnyCopy:   Number of evaluations (N = %7d): %d%n",L,nEvals);

	}


	private static int compareFirst(int[] a) {
		nEvals = 0;
		int result = -1;
		nEvals++;
		if (a[0]==a[1]) {
			result = 1;
		}
		return result;
	}
	
	private static int findFirstCopy(int[] a) {
		nEvals = 0;
		int result = -1;
		for (int i=1; i<a.length;i++) {
			nEvals++;
			if (a[0]==a[i]) {
				result = i;
				break;
			}
		}
		return result;
	}
	
	private static long findAnyCopy(int[] a) {
		nEvals = 0;
		long result = -1;
		for (int i=0; i<a.length;i++) {
			for (int j=i+1;j<a.length-1;j++) {
				nEvals++;
				if (a[i]==a[j]) {
					result = i;
					break;
				}
				// nEvals++; // Actually this line should be here. Why? 
				if (result!=-1) {break;} // try commenting this line...
			}
		}
		return result;
	}



}
