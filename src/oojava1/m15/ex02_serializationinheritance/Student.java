package oojava1.m15.ex02_serializationinheritance;
//From: https://www.javatpoint.com/serialization-in-java

class Student extends Person{  
	String course;  
    int fee;  
    
    public Student(int id, String name, String course, int fee) {  
    	super(id,name);  
    	this.course=course;  
    	this.fee=fee;  
    }  
}   