package oojava1.m15.ex02_serializationinheritance;
//From: https://www.javatpoint.com/serialization-in-java

import java.io.*;

public class SerializationDemo{  
	public final static String SAVEFILENAME = "src/oojava1/m15/ex02_serializationinheritance/f.txt";
	
	public static void main(String args[]){  
		try{  
			//Creating the object  
			Student s1 = new Student(211,"ravi","OOPwJava1",42);  
			//Creating stream and writing the object  
			FileOutputStream fout=new FileOutputStream(SAVEFILENAME);  
			ObjectOutputStream out=new ObjectOutputStream(fout);  
			out.writeObject(s1);  
			out.flush();  
			//closing the stream  
			out.close();  
			System.out.println("success");  
		} catch (Exception e) {
			System.out.println(e);
		}  
	}  
}  