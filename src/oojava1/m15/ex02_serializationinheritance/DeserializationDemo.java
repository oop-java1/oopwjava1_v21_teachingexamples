package oojava1.m15.ex02_serializationinheritance;
import java.io.*;

public class DeserializationDemo {
  
	public static void main(String args[]){  
		try {  
			//Creating stream to read the object  
			FileInputStream fin = new FileInputStream(SerializationDemo.SAVEFILENAME);
			ObjectInputStream in = new ObjectInputStream(fin);  
			Student s=(Student)in.readObject();  
			//printing the data of the serialized object  
			System.out.println(s.id+" "+s.name+" "+s.course+" "+s.fee);  
			//closing the stream  
			in.close();  
		} catch(Exception e) {
			System.out.println(e);
		}  
	}  
}  