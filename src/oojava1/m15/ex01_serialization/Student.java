package oojava1.m15.ex01_serialization;
//From: https://www.javatpoint.com/serialization-in-java

import java.io.Serializable; 

public class Student implements Serializable{  
	int id;  
	String name;
	
	public Student(int id, String name) {  
		this.id = id;  
		this.name = name;  
	}  
}  