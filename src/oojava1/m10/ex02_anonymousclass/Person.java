package oojava1.m10.ex02_anonymousclass;

import java.util.Date;

public class Person {
	protected Date birthday;
	protected String name;
	protected PersonAction personAction;
	
	private class PersonInnerClass {
		int x = 42;
	}
	
	public Person() {
		this("");
	}
	
	public Person(String name) {
		this.name = name;
		this.birthday = birthday;
	}


	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("My name is %s.",name);
	}
	
	public void sayMyName() {
		System.out.println("My name is "+this.name+".");
	}

	public PersonAction getPersonAction() {
		return personAction;
	}

	public void setPersonAction(PersonAction personAction) {
		this.personAction = personAction;
	}

	
	

}
