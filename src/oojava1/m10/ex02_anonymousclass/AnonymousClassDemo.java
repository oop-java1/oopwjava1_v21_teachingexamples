package oojava1.m10.ex02_anonymousclass;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;


public class AnonymousClassDemo {

	
	public static void main(String[] args) {
		PersonAction nelsonMandelaAction = new PersonAction() {
			@Override
			public void execute() {
				System.out.println("I came from South Africa.");
			}
		};
		
		Group group = new Group();
		Person person;
		person = group.add(new Person("Nelson Mandela"));
		person.setPersonAction(nelsonMandelaAction);
		
		person = group.add(new Person("Lord Nelson"));
		person.setPersonAction(new PersonAction() {
			@Override
			public void execute() {
				System.out.println("I was an English Admiral.");
			}
		});
		
		person = group.add(new Person("George Washington"));
		person = group.add(new Person("William Heinesen"));
		person = group.add(new Person("Anders And"));
		System.out.println(group.toString());
		System.out.println("Sorting...");
		group.sort();
		System.out.println(group.toString());
		
		System.out.println("...Performing actions...");
		for (Person p : group.getPersons()) {
			if (p.getPersonAction()!=null) {
				p.getPersonAction().execute();
			}
		}
	
	}


}
