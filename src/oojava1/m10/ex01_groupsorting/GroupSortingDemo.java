package oojava1.m10.ex01_groupsorting;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.bind.JAXB;

import oojava1.m03.ex04.FileLoader;


public class GroupSortingDemo {
	
	public static void main(String[] args) {
		String fileName = "src/oojava1/m09/ex02_xmlserialization_person/group.xml";
		Group group = new Group();
		group.add(new Person("Nelson Mandela"));
		group.add(new Person("Lord Nelson"));
		group.add(new Person("George Washington"));
		group.add(new Person("William Heinesen"));
		group.add(new Person("Anders And"));
		System.out.println(group.toString());
		System.out.println("Sorting...");
		group.sort();
		System.out.println(group.toString());
	
	}


}
