package oojava1.m10.ex01_groupsorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.bind.annotation.XmlElement;

public class Group {
	@XmlElement(name="person")
	private ArrayList<Person> persons = new ArrayList<Person>();
	
	public void add(Person person) {
		persons.add(person);
	}
	
	public int count() {
		return persons.size();
	}
	
	public String toString() {
		String result = "";
		for (int i  = 0; i<persons.size();i++) {
			result+=persons.get(i).toString()+"\n";
		}
		return result.trim();
	}
	
	public void sort() {
		
		Comparator personComparator = new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};
		
		Collections.sort(persons,personComparator);
		
	}
	
}
