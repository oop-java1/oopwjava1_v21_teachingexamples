package oojava1.m10.ex01_groupsorting;

import java.util.Date;

enum Gender {
	
	Male {
		public String toString() { return "male";}
	}, 
	Female {
		public String toString() { return "female";}
	}
}

public class Person {
	protected Date birthday;
	protected String name;
	protected Gender gender;
	
	
	public Person() {
		this("");
	}
	
	public Person(String name) {
		this(name,new Date(1918, 11, 11),Gender.Male);
	}
	
	public Person(String name, Date birthday, Gender gender) {
		this.name = name;
		this.birthday = birthday;
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return String.format("My name is %s.",name);
	}
	
	public void sayMyName() {
		System.out.println("My name is "+this.name+".");
	}

	
	

}
