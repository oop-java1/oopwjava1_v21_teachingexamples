package oojava1.m02.ex03_scenebuildertest_javafx;
// bug.png from https://storm.cis.fordham.edu/harazduk/cs3400/bug.png
public class WelcomeTest {

	public static void main(String[] args) {
		System.out.printf("WelcomeTest:\nOpen Welcome.fxml in Scene Builder, and run it from there.");

	}

}
