package oojava1.m12.ex01_functionalinterfacedemo;

import java.util.function.BiFunction;

public class FunctionalInterfaceDemo2 {

	public static void main(String[] args) {

		BiFunction<Double,Double,Double> feigenbaumElement = (Double a,Double x) -> {
			for (int i=0;i<1000;i++) {
				x=a*x*(1-x);
			}
			return x;
		};

		System.out.printf("Feigenbaum 2.2, 0.5, 1000   : %f%n",testBiFunction(2.2, 0.5, feigenbaumElement));
		System.out.printf("Feigenbaum 2.2, 0.5, 1000   : %f%n",testBiFunction(2.2, 0.5,
				(a,x) -> {for (int i=0;i<1000;i++) {x=a*x*(1-x);} return x;}));
		System.out.printf("Feigenbaum 2.7, 0.5, 1000   : %f%n",testBiFunction(2.7, 0.5, feigenbaumElement));
		System.out.printf("Feigenbaum 3.2, 0.5, 1000   : %f%n",testBiFunction(3.2, 0.5, feigenbaumElement));
		System.out.printf("Feigenbaum 3.7, 0.5, 1000   : %f%n",testBiFunction(3.7, 0.5, feigenbaumElement));
		System.out.printf("Feigenbaum 3.9, 0.5, 1000   : %f%n",testBiFunction(3.9, 0.5, feigenbaumElement));

	}

	private static double testBiFunction(double a, double x, BiFunction<Double, Double, Double> biFunc) {
		return biFunc.apply(a, x);
	}


	
}