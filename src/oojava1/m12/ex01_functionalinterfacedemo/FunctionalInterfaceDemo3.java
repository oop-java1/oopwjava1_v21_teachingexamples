package oojava1.m12.ex01_functionalinterfacedemo;

import java.util.Objects;
import java.util.function.Function;

public class FunctionalInterfaceDemo3 {
	
	// From: https://stackoverflow.com/questions/18400210/java-8-where-is-trifunction-and-kin-in-java-util-function-or-what-is-the-alt
	@FunctionalInterface
	interface TriFunction<A,B,C,R> {
	    R apply(A a, B b, C c);

	    default <V> TriFunction<A, B, C, V> andThen( 
	                                Function<? super R, ? extends V> after) {
	        Objects.requireNonNull(after);
	        return (A a, B b, C c) -> after.apply(apply(a, b, c));
	    }
	}

	public static void main(String[] args) {

		TriFunction<Double, Double, Integer, Double> feigenbaumElement = (Double a,Double x,Integer n) -> {
			for (int i=0;i<n;i++) {
				x=a*x*(1-x);
			}
			return x;
		};

		System.out.printf("Feigenbaum 1.2, 0.5, 1000   : %f%n",testTriFunction(1.2, 0.5, 1000,feigenbaumElement));
		System.out.printf("Feigenbaum 1.2, 0.5, 1001   : %f%n",testTriFunction(1.2, 0.5, 1001,feigenbaumElement));
		System.out.printf("Feigenbaum 2.2, 0.5, 1000   : %f%n",testTriFunction(2.2, 0.5, 1000,feigenbaumElement));
		System.out.printf("Feigenbaum 2.7, 0.5, 1000   : %f%n",testTriFunction(2.7, 0.5, 1000,feigenbaumElement));
		System.out.printf("Feigenbaum 3.2, 0.5, 1000   : %f%n",testTriFunction(3.2, 0.5, 1000,feigenbaumElement));
		System.out.printf("Feigenbaum 3.2, 0.5, 1001   : %f%n",testTriFunction(3.2, 0.5, 1001,feigenbaumElement));
		System.out.printf("Feigenbaum 3.7, 0.5, 1000   : %f%n",testTriFunction(3.7, 0.5, 1000,feigenbaumElement));
		System.out.printf("Feigenbaum 3.9, 0.5, 1000   : %f%n",testTriFunction(3.9, 0.5, 1000,feigenbaumElement));
		System.out.printf("Feigenbaum 3.9, 0.5, 1001   : %f%n",testTriFunction(3.9, 0.5, 1001,feigenbaumElement));
		System.out.printf("Feigenbaum 3.9, 0.5, 1002   : %f%n",testTriFunction(3.9, 0.5, 1002,feigenbaumElement));

	}

	private static double testTriFunction(double a, double x, int n, TriFunction<Double,Double,Integer,Double> triFun) {
		return triFun.apply(a, x, n);
	}	
}