package oojava1.m12.ex01_functionalinterfacedemo;

import java.util.function.BinaryOperator;
import java.util.function.Function;

public class FunctionalInterfaceDemo {

	public static void main(String[] args) {
		BinaryOperator<Double> addition = new BinaryOperator<Double>() { // Classic implementation of Interface
			@Override
			public Double apply(Double x, Double y) {
				return (Double) x+y;
			}
		};
		BinaryOperator<Double> multiplication = (x,y) -> x*y; // Lambda implementation of Interface

		
		Function<Double, Double> sqr = (x) -> x*x;
		Function<Double, Double> sqrt = (x) -> Math.sqrt(x);
		
		System.out.printf("Addition 3,4       : %f%n",testBinaryOperator(3,4,addition));
		System.out.printf("Multiplication 3,4 : %f%n",testBinaryOperator(3,4,multiplication));
		System.out.printf("Sqr 3              : %f%n",testFunction(3,sqr));
		System.out.printf("Sqrt 3             : %f%n",testFunction(3,sqrt));

	}

	private static double testBinaryOperator(double x, double y, BinaryOperator<Double> binOp) {
		return binOp.apply(x, y);
	}

	private static double testFunction(double x, Function<Double, Double> funcOp) {
		return funcOp.apply(x);
	}
	
}
