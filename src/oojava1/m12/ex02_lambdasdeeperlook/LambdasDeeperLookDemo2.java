package oojava1.m12.ex02_lambdasdeeperlook;

import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class LambdasDeeperLookDemo2 {
	
	public static void main(String[] args) {
		IntUnaryOperator intId = new IntUnaryOperator() {
			@Override
			public int applyAsInt(int operand) {
				return operand;
			}
		};
		
		int res = IntStream.rangeClosed(1, 10)
					   .filter(x -> x % 2 == 0)
					   .map(intId)
					   .sum();
		System.out.println("Result is "+res);
		
		int y = 2;
		res = IntStream.rangeClosed(1, 10)
				   .filter(x -> x % 2 == 0)
				   .map( (x) -> x*y)
				   .sum();
		System.out.println("Result is "+res);
	}

}
