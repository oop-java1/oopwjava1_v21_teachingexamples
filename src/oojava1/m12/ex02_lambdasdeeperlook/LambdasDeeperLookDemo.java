package oojava1.m12.ex02_lambdasdeeperlook;

import java.util.stream.IntStream;

public class LambdasDeeperLookDemo {

	public static void main(String[] args) {
		int res;
		res = IntStream.rangeClosed(1, 10)
				 	   .map((int x) -> {return x*2;})
				 	   .sum();
		System.out.println("Result is "+res);
		
		res = IntStream.rangeClosed(1, 10)	
					   .map(x -> x*2)
					   .sum();
		System.out.println("Result is "+res);
		
		res = IntStream.rangeClosed(1, 10)
					   .filter(x -> x % 2 == 0)
					   .map(x -> x*2)
					   .sum();
		System.out.println("Result is "+res);
		
		int y = 2;
		res = IntStream.rangeClosed(1, 10)
				   .filter(x -> x % 2 == 0)
				   .map( (x, y) -> x*y) // Find the error...
				   .sum();
		System.out.println("Result is "+res);
	}

}
