package oojava1.m12.ex03_streaminterger;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StreamIntegerDemo {

	public static void main(String[] args) {
		
		int[] ints  = {1,3,7,4,3,6,5,7,3,9};
		
		int res = Arrays.stream(ints)
			  .sorted()
			  .filter(x -> x>4)
			  .map(x -> x*2)
			  .sum();
		System.out.println("Result is "+res);
		
		Integer[] integers  = {1,3,7,4,3,6,5,7,3,9};
		List<Integer> res2 = Arrays.stream(integers)
				  .sorted()
				  .filter(x -> x>4)
				  .map(x -> (Integer) x*1)
				  .collect(Collectors.toList());
			System.out.println("Result is "+res2);
	

	}

}
