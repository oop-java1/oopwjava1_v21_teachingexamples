package oojava1.m12.ex04_inclasstests;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MapsTest {

	public static void main(String[] args) {

		CharSequence str = IntStream.rangeClosed(1, 10)
				  .map( x -> x*2)
				  .map(x -> x+1 )
				  .mapToObj(Integer::toString)
                  .collect(Collectors.joining(", "));

			System.out.println("Result is:\n"+str);
	

	}

	
}
