package oojava1.m01;

public class Exercise209 {

	public static void main(String[] args) {
		System.out.printf("a) Addition is performed first in the following statement: a * b / (c + d) * 5: True/False? %b\n",true);
		System.out.printf("   Because () has the highest arithmetic priority.\n\n"); 
		
		//int AccountValue, $value, value_in_$, account_no_1234, US$, her_sales_in_$, his_$checking_account, X!, _$_, a@b, _name;
		System.out.printf("b) The following are all valid variable names: AccountValue, $value, value_in_$, \n   account_no_1234, US$, her_sales_in_$, his_$checking_account, X!, _$_, a@b, _name. True/False? %b\n", false);
		System.out.printf("   Because ! and @ are not allowed in variable names.\n\n");
		
		System.out.printf("c) In 2 + 3 + 5 / 4, addition has the highest prioroity: True/False? %b\n",false);
		System.out.printf("   Because division has higher priority.\n\n"); 
		
		System.out.printf("d) The following are all invalid names: True/False? %b\n",true);
		System.out.printf("   Because ;-)\n\n"); 
		 
		
	}

}
