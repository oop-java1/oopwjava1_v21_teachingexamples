package oojava1.m13.ex03_folderexplorer;

import java.io.File;

public class FolderExplorer {
	
	public void traverse(String pathString) {
		File file = new File(pathString);
		if (file.isDirectory()) {
			System.out.println("+"+file.getPath());
			String[] files = file.list();
			for (String fn : files) {
				traverse(pathString+"/"+fn);
			}
		} else { // then it is a file - This is base case
			System.out.println("-"+file.getPath());
			
		}
	}

}
