package oojava1.m13.ex01_recursive;

public class Demo01Factorial {
	
	public int fact(int n) {
		if (n>1) {
			return n*fact(n-1);
		} else return 1;
	}

	public static void main(String[] args) {
		Demo01Factorial factorial = new Demo01Factorial();
		int number;
		number = 4;
		System.out.printf("%2d! = %d%n",number,factorial.fact(number));
		number = 5;
		System.out.printf("%2d! = %d%n",number,factorial.fact(number));
		number = 6;
		System.out.printf("%2d! = %d%n",number,factorial.fact(number));
		number = 10;
		System.out.printf("%2d! = %d%n",number,factorial.fact(number));

	}

}
