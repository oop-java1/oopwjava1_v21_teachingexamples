package oojava1.m13.ex01_recursive;

public class Demo02Recursive {
	
	public void loop(int n, int max) {
		System.out.printf("Printing a number: %d%n", n);
		if (n<max) {
			loop(++n,max);
//			loop(n++,max); // Try this line instead
		}
	}

	public static void main(String[] args) {
		Demo02Recursive demo2 = new Demo02Recursive();
		demo2.loop(0, 10);
	}

}
