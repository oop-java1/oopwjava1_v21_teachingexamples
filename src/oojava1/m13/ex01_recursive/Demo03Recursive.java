package oojava1.m13.ex01_recursive;

public class Demo03Recursive {
	
	public void fibonacci(int n1,int n2, int max) {
		if (n2<max) {
			System.out.print(" "+n2);
			fibonacci(n2, n1+n2, max);
		}
	}

	public static void main(String[] args) {
		Demo03Recursive demo3 = new Demo03Recursive();
		demo3.fibonacci(0,1,1000);
	}

}
