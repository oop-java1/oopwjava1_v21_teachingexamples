package oojava1.m11.ex01_simplestreams;

import java.util.stream.IntStream;

public class StreamMapOldFor {
  
	public static void main(String[] args) {
	      // sum the integers from 1 through 10
		int total  = 0;
		for (int i = 0; i <= 20; i+=2) {
			total += i;
		}
		System.out.printf("Sum of 1 through 10 is: %d%n",total);      
	}
}
