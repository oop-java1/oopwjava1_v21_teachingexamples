package oojava1.m08.ex06_interfaceinheritance;


public class SuperInterfaceTest {

	public static void main(String[] args) {
		SuperClass superClass = new SuperClass();
		SubClass subClass = new SubClass();
		
		if (subClass instanceof FancyInterface) {
			System.out.println("Yes");
		}

	}

}
