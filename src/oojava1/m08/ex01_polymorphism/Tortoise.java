package oojava1.m08.ex01_polymorphism;

public class Tortoise extends Animal {

	public void run(double time) {
		this.position += 0.5*time;
	}

}
