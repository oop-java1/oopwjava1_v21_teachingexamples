package oojava1.m08.ex01_polymorphism;

public class PolymorphismDemo {

	public static void main(String[] args) {
		Animal animal;

		animal = new Animal();
		animal.run(10);
		System.out.println(animal);
		
		animal = new Dog();
		animal.run(10);
		System.out.println(animal);
				
		animal = new Tortoise();
		animal.run(10);
		System.out.println(animal);

		animal = new Cow();
		animal.run(10);
		System.out.println(animal);

		animal =  new Bluewhale();
		animal.run(10);
		System.out.println(animal);
		
		
		
	}

}
