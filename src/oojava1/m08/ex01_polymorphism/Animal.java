package oojava1.m08.ex01_polymorphism;

public class Animal {
	protected double position = 0;
	
	public void run(double time) {
		this.position += 0*time;
	}
	
	public String toString() {
		return String.format("I am a %s and I am %.2f m from home.", this.getClass().getSimpleName(),position);
	}
	

}
