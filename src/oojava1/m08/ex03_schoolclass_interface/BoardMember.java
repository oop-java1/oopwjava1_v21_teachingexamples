package oojava1.m08.ex03_schoolclass_interface;

public class BoardMember extends Person implements Chauffeur {

	@Override
	public void talk() {
		System.out.println("\"As boardmembers we are too important to talk to our passengers!\"");
		
	}

}
