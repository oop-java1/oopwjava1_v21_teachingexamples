package oojava1.m08.ex03_schoolclass_interface;

public class Room {
	private String id;
	private String name;
	private int capacity;
	private String floor;
	private String address;
	private String building;
	private boolean curtains;
	private boolean projector;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public boolean isCurtains() {
		return curtains;
	}
	public void setCurtains(boolean curtains) {
		this.curtains = curtains;
	}
	public boolean isProjector() {
		return projector;
	}
	public void setProjector(boolean projector) {
		this.projector = projector;
	}
	
	
	
	

}
