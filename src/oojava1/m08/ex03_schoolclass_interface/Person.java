package oojava1.m08.ex03_schoolclass_interface;

import java.util.Date;

enum Gender {Male, Female, AnotherGender};

public class Person {
	protected String name;
	private Gender gender;
	private Date birthdate;
	
	public Person() {
		this("");
	}
	
	public Person(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	public void sayMyName() {
		System.out.println("My name is "+this.name+".");
	}
	
	public String toString() {
		return name;
	}

}
