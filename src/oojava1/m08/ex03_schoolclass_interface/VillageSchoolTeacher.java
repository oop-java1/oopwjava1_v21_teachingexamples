package oojava1.m08.ex03_schoolclass_interface;

public class VillageSchoolTeacher extends Teacher implements Chauffeur {

	public VillageSchoolTeacher(String name, String subject, double salary) {
		super(name,subject,salary);
	}
	
	@Override
	public void talk() {
		System.out.println("\"Right now I am driving a car, so teachers can drive cars too? Villageteachers have to...\"");
		
	}

}
