package oojava1.m08.ex03_schoolclass_interface;

public class ChangeHisMindPupil extends Pupil {
	
	public ChangeHisMindPupil(String name) {
		super(name);
	}
	
	@Override
	public void sayMyName() {
		super.sayMyName();
		System.out.println("... No.... sorry! My name is "+this.name+".");
	}

}
