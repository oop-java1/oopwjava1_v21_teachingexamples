package oojava1.m08.ex03_schoolclass_interface;

public class Janitor extends Person implements Chauffeur {

	@Override
	public void talk() {
		System.out.println("\"Yes Janitors drive cars too...\"");
	}

}
