package oojava1.m08.ex03_schoolclass_interface;

public class Car {
	
	public void drive(Chauffeur chauffeur) {
		System.out.println(chauffeur.getClass().getSimpleName()+ " is driving the car.");
		chauffeur.talk();
	}

}
