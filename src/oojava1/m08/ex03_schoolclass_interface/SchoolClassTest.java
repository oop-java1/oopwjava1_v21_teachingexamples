package oojava1.m08.ex03_schoolclass_interface;

public class SchoolClassTest {

	public static void main(String[] args) {
		SchoolClass schoolClass = new SchoolClass(1);
		VillageSchoolTeacher oojava1Teacher = new VillageSchoolTeacher("Jari","OOJava1",9999);
		schoolClass.setTeacher(oojava1Teacher);
		Janitor janitor = new Janitor();
		Car car = new Car();
		car.drive(oojava1Teacher);
		car.drive(janitor);
		car.drive(new BoardMember());


		

	}

}
