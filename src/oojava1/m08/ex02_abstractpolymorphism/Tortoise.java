package oojava1.m08.ex02_abstractpolymorphism;

public class Tortoise extends Animal {

	public void run(double time) {
		this.position += 0.5*time;
	}

}
