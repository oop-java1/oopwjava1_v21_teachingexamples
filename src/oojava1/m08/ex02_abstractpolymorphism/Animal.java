package oojava1.m08.ex02_abstractpolymorphism;

public abstract class Animal {
	protected double position = 0;
	
	public abstract void run(double time);
	
	public String toString() {
		return String.format("I am a %s and I am %.2f m from home.", this.getClass().getSimpleName(),position);
	}
	

}
