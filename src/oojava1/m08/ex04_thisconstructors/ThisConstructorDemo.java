package oojava1.m08.ex04_thisconstructors;

public class ThisConstructorDemo {

	public static void main(String[] args) {
		Cat cat;
		cat = new Cat();
		System.out.println(cat);

		cat = new Cat("Kiska");
		System.out.println(cat);
		
		
	}

}
