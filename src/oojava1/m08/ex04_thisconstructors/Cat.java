package oojava1.m08.ex04_thisconstructors;

public class Cat {
	private String name;
	
	public Cat() {
		this("Dronning Margrethe II"); // This is a call to the other constructor
	}
	
	public Cat(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
