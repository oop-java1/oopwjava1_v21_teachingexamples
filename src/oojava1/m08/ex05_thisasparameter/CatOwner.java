package oojava1.m08.ex05_thisasparameter;

public class CatOwner {
	private String name = "Jim";
	private Cat cat;
	
	public void setCat(Cat cat) {
		if (this.cat==null || !this.cat.equals(cat)) {
			this.cat = cat;
			if (this.cat!=null) {
				this.cat.setCatOwner(this);
			}
		}
	}
	
	@Override
	public String toString() {
		return name+" cat: "+((this.cat!=null) ? this.cat.getName() : "null");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Cat getCat() {
		return cat;
	}


}
