package oojava1.m08.ex05_thisasparameter;

public class ThisParameterDemo {

	public static void main(String[] args) {
		Cat cat = new Cat();
		CatOwner catOwner = new CatOwner();
		System.out.println(cat);
		System.out.println(catOwner);
		
		System.out.println("\n");
		cat.setCatOwner(catOwner);
		System.out.println(cat);
		System.out.println(catOwner);

	}

}
