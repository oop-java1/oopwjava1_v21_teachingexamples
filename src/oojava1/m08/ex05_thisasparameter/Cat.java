package oojava1.m08.ex05_thisasparameter;

public class Cat {
	private String name;
	private CatOwner catOwner;
	
	public Cat() {
		this("Dronning Margrethe II"); // This is a call to the other constructor
	}
	
	public Cat(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name+" owner: "+((catOwner!=null) ? this.catOwner.getName() : "null");
	}

	public CatOwner getCatOwner() {
		return catOwner;
	}

	public void setCatOwner(CatOwner catOwner) {
		if (!(this.catOwner!=null) || !this.catOwner.equals(catOwner)) {
//			System.out.println("Setting");
			this.catOwner = catOwner;
			if (this.catOwner!=null) {
				this.catOwner.setCat(this);
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	

}
